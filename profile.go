package main

import (
    "net/http"
    "fmt"
    "strings"
    "io/ioutil"
    "time"

    "github.com/sloonz/go-iconv"
    "code.google.com/p/go.net/html"
    "code.google.com/p/go-sqlite/go1/sqlite3"
)

var lo *time.Location
func main() {
    lo, _ = time.LoadLocation("Asia/Taipei")
    fetch()
}

func fetch() {
    const URL = "http://brk.twse.com.tw:8000/isin/class_main.jsp?chklike=Y&Page=1"
    resp, err := http.DefaultClient.Get(URL)
    cherr(err)
    txt, err := ioutil.ReadAll(resp.Body)
    cherr(err)
    str, err := iconv.Conv(string(txt), "utf8", "big5")
    cherr(err)
    doc, err := html.Parse(strings.NewReader(str))
    cherr(err)

    cs := make([]Company, 0)
    parse(doc, &cs)
    save(&cs)
}

type Company struct {
    ISIN	string
    No		string
    Name	string
    IPO		time.Time
    Industry	string
    CFI		string
    Type	string
    Market	string
}

func parse(node *html.Node, cs *[]Company) {
    if node.Type == html.ElementNode && node.Data == "tr" {
	c, _ := row(node)
	if c != nil {
	    *cs = append(*cs, *c)
	}
    }

    for c := node.FirstChild; c != nil; c = c.NextSibling {
	parse(c, cs)
    }
}

func row(node *html.Node) (*Company, error) {
    // ISIN
    td := node.FirstChild.NextSibling.NextSibling.NextSibling
    data := td.FirstChild.Data
    isin := data

    // No
    td = td.NextSibling.NextSibling
    data = strings.TrimSpace(td.FirstChild.Data)
    no := data

    // Name
    td = td.NextSibling.NextSibling
    data = td.FirstChild.Data
    name := data

    // Market
    td = td.NextSibling.NextSibling
    market := ""
    if td.FirstChild != nil {
	data = strings.TrimSpace(td.FirstChild.Data)
	market = data
    }

    // Type 
    td = td.NextSibling.NextSibling
    vtype := ""
    if td.FirstChild != nil {
	data = strings.TrimSpace(td.FirstChild.Data)
	vtype = data
    }

    // Industry
    td = td.NextSibling.NextSibling
    industry := ""
    if td.FirstChild != nil {
	data = td.FirstChild.Data
	industry = data
    }

    //IPO
    var y, m, d int
    td = td.NextSibling.NextSibling
    /*
    lo, err := time.LoadLocation("Asia/Taipei")
    cherr(err)
    */
    ipo := time.Date(0, time.Month(1), 1, 0, 0, 0, 0, lo)
    if td.FirstChild != nil {
	data = td.FirstChild.Data
	_, err := fmt.Sscanf(data, "%d/%d/%d", &y, &m, &d)
	if err != nil {
	    return nil, nil
	}
	ipo = time.Date(y, time.Month(m), d, 0, 0, 0, 0, lo)
    }

    //CFI
    td = td.NextSibling.NextSibling
    data = strings.TrimSpace(td.FirstChild.Data)
    cfi := data

    /*
    c := Company {
	ISIN: isin,
	No: no,
	Name: name,
	Market: market,
	Type: vtype,
	Industry: industry,
	IPO: ipo,
	CFI: cfi,
    }
    fmt.Printf("ISIN: %s, no: %s, name: %s, market: %s, type: %s, industry: %s, ipo: %d, cfi: %s\n", c.ISIN, c.No, c.Name, c.Market, c.Type, c.Industry, c.IPO.Unix(), c.CFI)
    return nil, nil
    */

    return &Company {
	ISIN: isin,
	No: no,
	Name: name,
	Market: market,
	Type: vtype,
	Industry: industry,
	IPO: ipo,
	CFI: cfi,
    }, nil
}

func save(cs *[]Company) {
    /*
    v := (*cs)[0]
    fmt.Printf("INSERT INTO raw_profile (ISIN, No, Name, IPO, CFI, Industry, Market, Type) VALUES ('%s', '%s', '%s', '%d', '%s', '%s', '%s', '%s');\n", v.ISIN, v.No, v.Name, v.IPO.Unix(), v.CFI, v.Industry, v.Market, v.Type)
    */

    conn, err := sqlite3.Open("data/db")
    cherr(err)
    defer conn.Close()
    conn.Begin()
    for _, v := range *cs {
	stmt := fmt.Sprintf("INSERT INTO raw_profile (ISIN, No, Name, IPO, CFI, Industry, Market, Type) VALUES ('%s', '%s', '%s', '%d', '%s', '%s', '%s', '%s');\n", v.ISIN, v.No, v.Name, v.IPO.Unix(), v.CFI, v.Industry, v.Market, v.Type)
	err = conn.Exec(stmt)
    }
    conn.Commit()
}

func cherr(err error) {
    if err != nil {
	panic(err)
    }
}
