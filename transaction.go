package main

import (
    "encoding/csv"
    "fmt"
    "strings"
    "time"
    "strconv"
    "io"
    "io/ioutil"
    "net/http"
    "runtime"
    "path/filepath"
    "log"

    "github.com/sloonz/go-iconv"
    "code.google.com/p/go-sqlite/go1/sqlite3"
)

var lo *time.Location
func main() {
    var err error
    lo, err = time.LoadLocation("Asia/Taipei")
    cherr(err)
    runtime.GOMAXPROCS(4)
    //open()
    daily()
}

type Transaction struct {
    No		    string
    Date	    time.Time
    TradeShares	    uint64
    TradeSharesK    uint64
    TradeSum	    uint64
    Open	    float32
    Highest	    float32
    Lowest	    float32
    Price	    float32
    Range	    float32
}

func open() {
    const basePath = "data/transactions/3545/"
    fis, err := ioutil.ReadDir(basePath)
    cherr(err)

    //slice := make([]Transaction, 0)
    for _, v := range fis {
	path := filepath.Join(basePath, v.Name())
	b, err := ioutil.ReadFile(path)
	cherr(err)
	str, err := iconv.Conv(string(b), "utf8", "big5")
	cherr(err)
	// Skip header
	seek := strings.LastIndex(str, "筆數") + 6
	reader := strings.NewReader(str[seek:])
	t := parseList("3545", reader)
	for _, vv := range t {
	    fmt.Printf("INSERT INTO raw_transaction (No, Date, TradeShares, TradeSum, TradeSharesK, Open, Highest, Lowest, Price, Range) VALUES ('%s', %d, %d, %d, %d, %f, %f, %f, %f, %f);\n", vv.No, vv.Date.Unix(), vv.TradeShares, vv.TradeSum, vv.TradeSharesK, vv.Open, vv.Highest, vv.Lowest, vv.Price, vv.Range)
	}
	//slice = append(slice, t...)
    }
}

func fetch() {
    prepareFetch()
}

type Prepare struct {
    No string
    IPO time.Time
}

func prepareFetch() {
    // Prepare fetch
    conn, err := sqlite3.Open("data/db")
    cherr(err)
    defer conn.Close()

    // TWSE records begins on 1993-01-04, epoch time 726076800.
    sql := "SELECT No, IPO FROM raw_profile WHERE Market='上市' AND Type='股票'"
    slice := make([]Prepare, 0)
    for rs, err := conn.Query(sql); err == nil; err = rs.Next() {
	var no string
	var val int
	rs.Scan(&no, &val)

	begin := time.Unix(int64(val), 0) 
	if val < 726076800 {
	    begin = time.Date(1993, time.Month(1), 1, 0, 0, 0, 0, lo)
	}
	slice = append(slice, Prepare {
	    No: no,
	    IPO: begin,
	})
    }

    for _, v := range slice {
	slice := make([]Transaction, 0)
	now := time.Now()
	for date := v.IPO; date.Before(now); date = date.AddDate(0, 1, 0) {
	    t := fetchData(v.No, date)
	    slice = append(slice, t...)
	    log.Printf("%s %d-%02d\n", v.No, date.Year(), date.Month())
	}
	save(slice)
	log.Printf("Save %s done.\n", v.No)
    }
}

func daily() {
    /* FIXME
	1. Duplicated records with same date, where no is 1337.
	2. Return records by channel, batch writing to db.
    */
    // Prepare fetch
    conn, err := sqlite3.Open("data/db")
    cherr(err)
    defer conn.Close()

    // TWSE records begins on 1993-01-04, epoch time 726076800.
    sql := "SELECT No FROM raw_profile WHERE Market='上市' AND Type='股票'"
    slice := make([]Prepare, 0) 
    for rs, err := conn.Query(sql); err == nil; err = rs.Next() {
	var no string
	rs.Scan(&no)

	slice = append(slice, Prepare {
	    No: no,
	})
    }

    //thatTime := time.Date(2013, time.Month(8), 27, 0, 0, 0, 0, lo)
    now := time.Now()
    for _, v := range slice {
	ts := fetchData(v.No, now)
	t := strip(ts)
	save(t)
	log.Printf("Save %s done.\n", v.No)
    }
}

func strip(ts []Transaction) ([]Transaction) {
    now := time.Now()
    //now := time.Date(2013, time.Month(8), 27, 0, 0, 0, 0, lo)
    slice := make([]Transaction, 0)
    for _, v := range ts {
	if v.Date.Year() == now.Year() &&
	    v.Date.Month() == now.Month() &&
	    v.Date.Day() <= now.Day() {
		slice = append(slice, v)
	}
    }
    return slice
}

func fetchData(no string, date time.Time) ([]Transaction) {
    y := date.Year()
    m := date.Month()

    url := fmt.Sprintf("http://www.twse.com.tw/ch/trading/exchange/STOCK_DAY/STOCK_DAY_print.php?genpage=genpage/Report%d%02d/%d%02d_F3_1_8_%s.php&type=csv", y, m, y, m, no)
    /*
    fmt.Printf("%s\n", url)
    return make([]Transaction, 0)

    */
    resp, err := http.DefaultClient.Get(url)
    cherr(err)
    raw, _ := ioutil.ReadAll(resp.Body)
    if len(raw) <= 77 {
	log.Printf("%s has empty records in %d-%02d\n", no, y, m)
	return nil
    }

    str, err := iconv.Conv(string(raw), "utf8", "big5")
    cherr(err)
    // Skip header
    seek := strings.LastIndex(str, "筆數") + 6
    reader := strings.NewReader(str[seek:])
    return parseList(no, reader)
}

func parseList(no string, ioreader io.Reader) ([]Transaction) {
    reader := csv.NewReader(ioreader)
    rs, err := reader.ReadAll()
    cherr(err)
    slice := make([]Transaction, 0)
    for _, v := range rs {
	t, err  := parse(no, v)
	if err == nil {
	    slice = append(slice, t)
	} else {
	    log.Fatalf("%s has error, %s\n", no, err.Error())
	    break
	}
    }
    return slice
}

func parse(no string, item []string) (Transaction, error) {
    // Date
    var y, m, d int
    _, err := fmt.Sscanf(item[0], "%d/%d/%d", &y, &m, &d)
    if err != nil {
	return Transaction{}, err
    }
    date := time.Date(1911 + y, time.Month(m), d, 0, 0, 0, 0, lo)

    // Trade Shares
    data := strings.Trim(strings.Replace(item[1], ",", "", -1), "\"")
    tradeshares, err := strconv.ParseUint(data, 10, 64)
    if err != nil {
	return Transaction{}, err
    }

    // Trade sum
    data = strings.Trim(strings.Replace(item[2], ",", "", -1), "\"")
    tradesum, err := strconv.ParseUint(data, 10, 64)
    if err != nil {
	return Transaction{}, err
    }

    // Open
    value, err := strconv.ParseFloat(item[3], 32)
    open := float32(0)
    if err == nil {
	open = float32(value)
    }

    // Highest
    value, err = strconv.ParseFloat(item[4], 32)
    highest := float32(0.0)
    if err == nil {
	highest = float32(value)
    }

    // Lowest
    value, err = strconv.ParseFloat(item[5], 32)
    lowest := float32(0.0)
    if err == nil {
	lowest = float32(value)
    }

    // Price
    value, err = strconv.ParseFloat(item[6], 32)
    price := float32(0.0)
    if err == nil {
	price = float32(value)
    }

    // Range, reserved keywords
    vrange := float32(0.0)
    // First record in twse, no compared range with last day.
    if item[7] != "X0.00" {
	value, err = strconv.ParseFloat(item[7], 32)
	if err != nil {
	    return Transaction{}, err
	}
	vrange = float32(value)
    }

    // Trade Shares in K
    data = strings.Trim(strings.Replace(item[8], ",", "", -1), "\"")
    tradesharesk, err := strconv.ParseUint(data, 10, 64)
    if err != nil {
	return Transaction{}, err
    }

    return Transaction {
	No: no,
	Date: date,
	TradeShares: tradeshares,
	TradeSharesK: tradesharesk,
	TradeSum: tradesum,
	Open: open,
	Highest: highest,
	Lowest: lowest,
	Price: price,
	Range: vrange,
    }, nil
}

func save(transactions []Transaction) {
    conn, err := sqlite3.Open("data/db")
    cherr(err)

    err = conn.Begin()
    cherr(err)
    for _, t := range transactions {
	//fmt.Printf("INSERT INTO raw_transaction (No, Date, TradeShares, TradeSum, TradeSharesK, Open, Highest, Lowest, Price, Range) VALUES ('%s', %d, %d, %d, %d, %f, %f, %f, %f, %f);\n", t.No, t.Date.Unix(), t.TradeShares, t.TradeSum, t.TradeSharesK, t.Open, t.Highest, t.Lowest, t.Price, t.Range)
	stmt := fmt.Sprintf("INSERT INTO raw_transaction (No, Date, TradeShares, TradeSum, TradeSharesK, Open, Highest, Lowest, Price, Range) VALUES ('%s', %d, %d, %d, %d, %f, %f, %f, %f, %f);", t.No, t.Date.Unix(), t.TradeShares, t.TradeSum, t.TradeSharesK, t.Open, t.Highest, t.Lowest, t.Price, t.Range)
	err := conn.Exec(stmt)
	cherr(err)
    }
    err = conn.Commit()
    cherr(err)
    err = conn.Close()
    cherr(err)
}

func cherr(err error) {
    if err != nil {
	panic(err)
    }
}
